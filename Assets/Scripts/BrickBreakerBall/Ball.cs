using UnityEngine;
using Zenject;

namespace Assets.Scripts.BrickBreakerBall
{

    public class Ball : MonoBehaviour, IBall
    {
        public Vector3 Position => transform.position;

        [SerializeField] private Vector3 _startForce;
        [SerializeField] private Rigidbody2D _rigidbody;

        public void StartBallMovement()
        {
            transform.parent = null;
            _rigidbody.AddForce(_startForce);
        }

        public void SetBallDirection(Vector3 direction)
        {
            _rigidbody.velocity = direction * _rigidbody.velocity.magnitude;
        }

        public void Reset(Transform transform)
        {
            transform.parent = transform;
            _rigidbody.velocity = Vector3.zero;
        }

        public class Pool : MonoMemoryPool<Ball>
        {

        }
    }
}