﻿using UnityEngine;

namespace Assets.Scripts.BrickBreakerBall
{
    public interface IBall
    {
        Vector3 Position { get; }

        void StartBallMovement();
        void SetBallDirection(Vector3 direction);
        void Reset(Transform transform);
    }
}