using Assets.Scripts.Borders;
using Assets.Scripts.BrickBreakerBall;
using Assets.Scripts.Bricks;
using Assets.Scripts.Bricks.BricksCollectionService;
using Assets.Scripts.Bricks.BricksConfigurations;
using Assets.Scripts.Bricks.BricksMovement;
using Assets.Scripts.Bricks.BricksSpawn;
using Assets.Scripts.Player;
using Assets.Scripts.Score;
using Assets.Scripts.Score.Services;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{
    public class GameSceneInstaller : MonoInstaller
    {
        private const int BricksInitialPoolSize = 30;
        private const int BallsInitialPoolSize = 10;

        [SerializeField] private PlayerInteractionsController _playerController;
        [SerializeField] private Ball _ball;
        [SerializeField] private Brick _brickPrefab;
        [SerializeField] private ScoreDisplay _scoreDisplay;
        [SerializeField] private BottomBorder _bottomBorder;

        [Header("Data")]
        [SerializeField] private BricksConfiguration _bricksConfiguration;
        [SerializeField] private BricksMovementConfiguration _brickMovementConfiguration;
        [SerializeField] private BrickSpawnData _brickSpawnData;

        public override void InstallBindings()
        {
            Container.Bind<PlayerInteractionsController>().FromInstance(_playerController);
            Container.Bind<BricksConfiguration>().FromInstance(_bricksConfiguration);
            Container.Bind<BricksMovementConfiguration>().FromInstance(_brickMovementConfiguration);
            Container.Bind<BrickSpawnData>().FromInstance(_brickSpawnData);
            Container.Bind<ScoreDisplay>().FromInstance(_scoreDisplay);
            Container.Bind<BottomBorder>().FromInstance(_bottomBorder);

            Container.BindInterfacesTo<Ball>().FromInstance(_ball);

            Container.BindInterfacesTo<PlayerInputService>().AsSingle().NonLazy();
            Container.BindInterfacesTo<BrickSpawnService>().AsSingle().NonLazy();
            Container.BindInterfacesTo<BricksMovementService>().AsSingle().NonLazy();
            Container.BindInterfacesTo<BrickCollectionService>().AsSingle();
            Container.BindInterfacesTo<ScoreService>().AsSingle();

            Container.BindMemoryPool<Brick, Brick.Pool>()
                .WithInitialSize(BricksInitialPoolSize)
                .FromComponentInNewPrefab(_brickPrefab);

            Container.BindMemoryPool<Ball, Ball.Pool>()
                .WithInitialSize(BallsInitialPoolSize)
                .FromComponentInNewPrefab(_ball);
        }
    }
}