using Assets.Scripts.Score.Services;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Score
{
    public class ScoreDisplay : MonoBehaviour
    {
        private const string ScorePrefix = "Score: ";

        [SerializeField] private TextMeshProUGUI _scoreText;

        private IScoreService _scoreService;

        [Inject]
        public void Initialize(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }

        private void Start()
        {
            _scoreService.PlayerScoreUpdated += OnPlayerScoreUpdated;
            UpdatePlayerScore();
        }

        private void OnDestroy()
        {
            _scoreService.PlayerScoreUpdated -= OnPlayerScoreUpdated;
        }

        private void OnPlayerScoreUpdated() => UpdatePlayerScore();

        private void UpdatePlayerScore() => _scoreText.text = $"{ScorePrefix}{_scoreService.PlayerScore}";
    }
}