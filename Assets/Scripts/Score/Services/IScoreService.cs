﻿using System;

namespace Assets.Scripts.Score.Services
{
    public interface IScoreService
    {
        event Action PlayerScoreUpdated;

        int PlayerScore { get; }

        public void AddScore(int score);
    }
}