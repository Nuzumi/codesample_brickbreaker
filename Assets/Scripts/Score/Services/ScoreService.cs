using System;

namespace Assets.Scripts.Score.Services
{

    public class ScoreService : IScoreService
    {
        public event Action PlayerScoreUpdated;

        public int PlayerScore { get; private set; }

        public void AddScore(int score)
        {
            PlayerScore += score;
            PlayerScoreUpdated?.Invoke();
        }
    }
}