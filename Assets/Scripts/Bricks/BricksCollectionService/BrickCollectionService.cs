using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Bricks.BricksCollectionService
{
    public interface IBrickCollectionService
    {
        IReadOnlyList<Brick> ActiveBricks { get; }

        void AddBrick(Brick brick);
        void RemoveBrick(Brick brick);
    }

    public class BrickCollectionService : IBrickCollectionService
    {
        public IReadOnlyList<Brick> ActiveBricks => _activeBricks;

        private readonly List<Brick> _activeBricks = new();

        public void AddBrick(Brick brick) => _activeBricks.Add(brick);

        public void RemoveBrick(Brick brick) => _activeBricks.Remove(brick);
    }
}