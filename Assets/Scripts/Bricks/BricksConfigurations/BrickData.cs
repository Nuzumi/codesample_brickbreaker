﻿using System;
using UnityEngine;

namespace Assets.Scripts.Bricks.BricksConfigurations
{

    [Serializable]
    public class BrickData : IBrickConfiguration
    {
        [field: SerializeField] public int Hp { get; private set; }
        [field: SerializeField] public int Score { get; private set; }
        [field: SerializeField] public Color Color { get; private set; }
        [field: SerializeField] public int SpawnWeight { get; private set; }
    }
}