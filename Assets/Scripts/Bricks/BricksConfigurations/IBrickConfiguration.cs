﻿using UnityEngine;

namespace Assets.Scripts.Bricks.BricksConfigurations
{
    public interface IBrickConfiguration
    {
        int Hp { get; }
        int Score { get; }
        Color Color { get; }
    }
}