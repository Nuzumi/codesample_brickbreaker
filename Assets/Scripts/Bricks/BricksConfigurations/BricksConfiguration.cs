using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Bricks.BricksConfigurations
{
    [CreateAssetMenu(menuName = "Data/" + nameof(BricksConfiguration))]
    public class BricksConfiguration : ScriptableObject
    {
        [field: SerializeField] public float BrickHeight { get; private set; }
        [field: SerializeField] public List<BrickData> BricksData { get; private set; }
    }
}