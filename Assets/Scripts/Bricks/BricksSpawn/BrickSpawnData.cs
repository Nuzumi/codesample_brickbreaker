using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Bricks.BricksSpawn
{
    public class BrickSpawnData : MonoBehaviour
    {
        public float BrickSpawnYPosition => _brickSpawnPoints.First().position.y;

        public List<Vector3> GetBricksSpawnPositions() => _brickSpawnPoints.Select(b => b.position).ToList();

        [SerializeField] private List<Transform> _brickSpawnPoints;
    }
}