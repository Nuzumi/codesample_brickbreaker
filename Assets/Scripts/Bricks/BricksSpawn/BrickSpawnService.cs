using Assets.Scripts.Bricks.BricksCollectionService;
using Assets.Scripts.Bricks.BricksConfigurations;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Bricks.BricksSpawn
{

    public class BrickSpawnService : ITickable
    {
        private readonly BricksConfiguration _bricksConfiguration;
        private readonly Brick.Pool _brickPool;
        private readonly IBrickCollectionService _brickCollectionService;
        private readonly List<Vector3> _bricksSpawnPositions;
        private readonly float _brickSpawnYPosition;
        private readonly int _bricksConfigurationWeightSum;

        public BrickSpawnService(BrickSpawnData brickSpawnData, BricksConfiguration bricksConfiguration, Brick.Pool brickPool, IBrickCollectionService brickCollectionService)
        {
            _bricksConfiguration = bricksConfiguration;
            _brickPool = brickPool;
            _brickCollectionService = brickCollectionService;
            _bricksSpawnPositions = brickSpawnData.GetBricksSpawnPositions();
            _brickSpawnYPosition = brickSpawnData.BrickSpawnYPosition;
            _bricksConfigurationWeightSum = bricksConfiguration.BricksData.Sum(c => c.SpawnWeight);
        }

        public void Tick()
        {
            if (CanSpawnBrickRow())
            {
                SpawnBrickRow();
            }
        }

        private void SpawnBrickRow()
        {
            _bricksSpawnPositions.ForEach(SpawnBrick);
        }

        private void SpawnBrick(Vector3 possition)
        {
            var brick = _brickPool.Spawn();
            brick.Configure(GetWeightedRandomBrickConfiguration(), possition);
            _brickCollectionService.AddBrick(brick);
        }

        private IBrickConfiguration GetWeightedRandomBrickConfiguration()
        {
            var weight = Random.Range(0, _bricksConfigurationWeightSum);
            foreach (var brickConfiguration in _bricksConfiguration.BricksData)
            {
                weight -= brickConfiguration.SpawnWeight;

                if(weight <= 0)
                {
                    return brickConfiguration;
                }
            }

            return _bricksConfiguration.BricksData.Last();
        }

        private bool CanSpawnBrickRow()
        {
            if (!_brickCollectionService.ActiveBricks.Any())
            {
                return true;
            }

            var maxBrickY = _brickCollectionService.ActiveBricks.Max(b => b.transform.position.y);
            return maxBrickY + _bricksConfiguration.BrickHeight < _brickSpawnYPosition;
        }
    }
}