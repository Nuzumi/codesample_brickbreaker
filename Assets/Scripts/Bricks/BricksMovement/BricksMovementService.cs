using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Bricks.BricksMovement
{
    public interface IBricksMovementService
    {
        void StartBrickMovement(Brick brick);
        void StopBrickMovement(Brick brick);
    }

    public class BricksMovementService : ITickable, IBricksMovementService
    {
        public float CurrentBricksSpeed { get; private set; }

        private readonly BricksMovementConfiguration _bricksMovementConfiguration;

        private List<Brick> _bricks = new();
        private float _lastSpeedUpdateTime;

        public BricksMovementService(BricksMovementConfiguration bricksMovementConfiguration)
        {
            _bricksMovementConfiguration = bricksMovementConfiguration;
        }

        public void Tick()
        {
            MoveBricks();

            if (CanIncreaseSpeed())
            {
                _lastSpeedUpdateTime = Time.time;
                IncreaseSpeed();
            }
        }

        public void StartBrickMovement(Brick brick)
        {
            _bricks.Add(brick);
        }

        public void StopBrickMovement(Brick brick)
        {
            _bricks.Remove(brick);
        }

        private void MoveBricks()
        {
            var moveVector = new Vector3(0, -CurrentBricksSpeed * Time.deltaTime, 0);

            foreach (var brick in _bricks)
            {
                brick.transform.position += moveVector;
            }
        }

        private bool CanIncreaseSpeed()
        {
            if(CurrentBricksSpeed >= _bricksMovementConfiguration.EndSpeed)
            {
                return false;
            }

            return Time.time >= _lastSpeedUpdateTime + _bricksMovementConfiguration.SpeedIncreaseInterval;
        }

        private void IncreaseSpeed()
        {
            CurrentBricksSpeed += _bricksMovementConfiguration.SpeedIncreaseStep;
            CurrentBricksSpeed = Mathf.Min(CurrentBricksSpeed, _bricksMovementConfiguration.EndSpeed);
        }
    }
}