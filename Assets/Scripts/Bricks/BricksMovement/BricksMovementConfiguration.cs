using UnityEngine;

namespace Assets.Scripts.Bricks.BricksMovement
{
    [CreateAssetMenu(menuName = "Data/" + nameof(BricksMovementConfiguration))]
    public class BricksMovementConfiguration : ScriptableObject
    {
        [field: SerializeField] public float StartSpeed { get; private set; }
        [field: SerializeField] public float EndSpeed { get; private set; }
        [field: SerializeField] public float SpeedIncreaseStep { get; private set; }
        [field: SerializeField] public float SpeedIncreaseInterval { get; private set; }
    }
}