using Assets.Scripts.BrickBreakerBall;
using Assets.Scripts.Bricks.BricksCollectionService;
using Assets.Scripts.Bricks.BricksConfigurations;
using Assets.Scripts.Bricks.BricksMovement;
using Assets.Scripts.Score.Services;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Bricks
{
    public class Brick : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;

        private IBricksMovementService _movementService;
        private IBrickCollectionService _brickCollectionService;
        private IScoreService _scoreService;
        private Pool _pool;
        private int _hp;
        private int _score;

        [Inject]
        public void Initialize(IBricksMovementService bricksMovementService, IBrickCollectionService brickCollectionService, IScoreService scoreService, Pool pool)
        {
            _movementService = bricksMovementService;
            _brickCollectionService = brickCollectionService;
            _scoreService = scoreService;
            _pool = pool;
        }

        public void Configure(IBrickConfiguration configuration, Vector3 position)
        {
            _hp = configuration.Hp;
            _score = configuration.Score;
            _spriteRenderer.color = configuration.Color;
            transform.position = position;

            _movementService.StartBrickMovement(this);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<IBall>() == null)
            {
                return;
            }

            OnBallCollision();
        }

        private void OnBallCollision()
        {
            _hp--;
            //todo connection to score service
            if(_hp <= 0)
            {
                _movementService.StopBrickMovement(this);
                _brickCollectionService.RemoveBrick(this);
                _scoreService.AddScore(_score);
                _pool.Despawn(this);
            }
        }

        public class Pool : MonoMemoryPool<Brick>
        {

        }
    }
}