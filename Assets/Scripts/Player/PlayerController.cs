using Assets.Scripts.BrickBreakerBall;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Player
{

    public class PlayerInteractionsController : MonoBehaviour
    {
        [SerializeField] private float _speed;//todo move to player data service
        [SerializeField] private float _minPossition;
        [SerializeField] private float _maxPossition;
        [SerializeField] private Transform _ballParent;

        private IPlayerInputService _inputService;
        private IBall _ball;

        [Inject]
        public void Initialize(IPlayerInputService playerInputService, IBall ball)
        {
            _inputService = playerInputService;
            _ball = ball;

            _inputService.PlayerShoot += OnPlayerShoot;
        }

        private void OnDisable()
        {
            _inputService.PlayerShoot -= OnPlayerShoot;
        }

        private void Update()
        {
            transform.position += new Vector3(_speed * Time.deltaTime * _inputService.PlayerMoveValue, 0, 0);
            var possitionX = Mathf.Clamp(transform.position.x, _minPossition, _maxPossition);
            transform.position = new Vector3(possitionX, transform.position.y, transform.position.z);
        }

        private void OnPlayerShoot() => _ball.StartBallMovement();
    }
}