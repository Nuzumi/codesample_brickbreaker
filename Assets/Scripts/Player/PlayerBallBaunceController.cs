﻿using Assets.Scripts.BrickBreakerBall;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerCollisionsController : MonoBehaviour
    {
        [SerializeField] private float _platformRadius;//todo move to player data service
        [SerializeField] private Vector3 _leftEdgeBallDirection;
        [SerializeField] private Vector3 _rightEdgeBallDirection;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            
            if(collision.gameObject.TryGetComponent<IBall>(out var ball))
            {
                OnBallCollision(ball);
            }
        }

        private void OnBallCollision(IBall ball)
        {
            var ballOffset = (ball.Position - transform.position).x;
            var ballOffsetNormalized = (ballOffset / _platformRadius + 1) / 2;
            var ballDirection = Vector3.Lerp(_leftEdgeBallDirection, _rightEdgeBallDirection, ballOffsetNormalized);

            ball.SetBallDirection(ballDirection.normalized);
        }
    }
}