using System;
using Zenject;

namespace Assets.Scripts.Player
{
    public interface IPlayerInputService
    {
        event Action PlayerShoot;
        float PlayerMoveValue { get; }
    }

    public class PlayerInputService : IPlayerInputService, IInitializable, IDisposable
    {
        public event Action PlayerShoot;

        public float PlayerMoveValue { get; private set; }

        private InputMaps.PlayerActions _playerActions;

        public void Initialize()
        {
            _playerActions = new InputMaps().Player;
            _playerActions.Enable();
            Subscribe();
        }

        public void Dispose()
        {
            _playerActions.Disable();
        }

        private void Subscribe()
        {
            _playerActions.Move.performed += ctx => PlayerMoveValue = ctx.ReadValue<float>();
            _playerActions.Move.canceled += ctx => PlayerMoveValue = 0;

            _playerActions.ShootBall.performed += _ => PlayerShoot?.Invoke();
        }
    }
}