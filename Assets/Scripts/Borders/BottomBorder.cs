using Assets.Scripts.BrickBreakerBall;
using Assets.Scripts.Bricks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scripts.Borders
{
    public class BottomBorder : MonoBehaviour
    {
        private Ball.Pool _ballPool;

        [Inject]
        public void Initialize(Ball.Pool ballPool)
        {
            _ballPool = ballPool;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.gameObject.TryGetComponent<Ball>(out var ball))
            {
                _ballPool.Despawn(ball);
                return;
            }

            if(collision.gameObject.TryGetComponent<Brick>(out var _))
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}